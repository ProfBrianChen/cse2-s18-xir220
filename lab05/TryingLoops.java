//Trying and playing around with loops
import java.util.Scanner;

public class TryingLoops{

    public static void main(String args[])
    {
      Scanner alex = new Scanner(System.in);
      Double test;
      System.out.println("What number are you going to put (positive intenger por favor):");
      while(true) 
      {
        test = alex.nextDouble();
        if (test == 9)
        {
          System.out.println("eat");
        }
        else
        {
          System.out.println("do not eat");
          break;
        }
      }
    }
}