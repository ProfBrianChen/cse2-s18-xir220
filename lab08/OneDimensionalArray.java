//CSE 2 Xaymara Rivera 
// 4.6.2018 lab08
import java.util.Scanner;
public class OneDimensionalArray {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);
    int Num_Students = (int) (Math.random( )* 6) +5;
    boolean done = true;
    System.out.println("The amount of Students: " +Num_Students);
    String[] students = new String[Num_Students];
    for (int i = 0; i < Num_Students; i++)
    {
      students[i] = myScanner.nextLine();
    }
    int[] midterm = new int [Num_Students];
    for (int i = 0; i < Num_Students; i++)
    {
      midterm[i]=(int) (Math.random()*100);
      System.out.println("These are the scores for "+students[i]+ ": "+midterm[i]);
    }
  }//end of main method
}//end of program