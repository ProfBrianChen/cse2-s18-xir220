//CSE 2 
import java.util.Scanner;
public class Bycicle {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Enter the number of seconds:");
    int numSec = myScanner.nextInt();
    System.out.print("Enter the number of counts:");
    int numCounts = myScanner.nextInt();
    int secstoMin = 60;
    int numMins = numSec / secstoMin;
    double WheelDiameter = 27.0, PI = 3.14159 , feetperMile = 5280 , inchesperFoot = 12;
    double distance = numCounts * WheelDiameter * PI;
    distance /= (inchesperFoot * feetperMile);
    System.out.println("The distance was "+distance+" and took "+numMins);
    int minstoHour = 60;
    double averagemph = ((distance *minstoHour) /(numMins));
    System.out.println("The average mph was "+averagemph);   
  }
}