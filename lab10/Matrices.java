//CSE 2 Xaymara Rivera
//4.20.18 lab10

public class Matrices {
  public static void main(String[] args) {
    
    int width= (int) (Math.random()*11)+1;
    int height= (int) (Math.random()*11)+1;
    boolean format = true;
    int [][] array = increasingMatrix(width, height, format);
    int [][] b = increasingMatrix(width,height, !format);
    printMatrix (array,format);
    System.out.println(" ");
    printMatrix(b, !format);
    System.out.println(" ");
    int newwidth = (int) (Math.random()*11)+1;
    int newheight = (int) (Math.random()*11)+1;
    int [][] c = increasingMatrix(newwidth,newheight,format);
    printMatrix(c, format);
    
  }//end of main method 
  
  public static int [][] increasingMatrix (int width, int height, boolean format) 
  {

    int [][] array; //new int [height] [width];
    
    if (format)
    {
      array = new int [height] [width];
      int counter = 0;
       for (int i = 0; i < height; i++ )
    {
     
          for (int j = 0; j < width; j++)
          {
            array[i][j]= counter;
            counter++;
            
          }//row
         
    }//column
    }//row-major representation
    
    
    
    
    else 
    {
      array = new int [width] [height];
      int counter = 0;
      int columncounter = 0;
       for (int i = 0; i < height; i++ )
    {
          for (int j = 0; j < width; j++)
          {
            array[j][i]= counter;
            counter++;
            
          }//row
         //System.out.println("");
         
         //columncounter++;
         //counter=columncounter;
         
    }//column
    } //column-major representation
    return array;
  } //end of increasingMatrix
  
  
  
  
  
  public static void printMatrix ( int[][] array,boolean format)
  {
    if (array==null)
    {
      System.out.println("The array was empty!");
      return;
    }
    
    int height= array.length;
    int width= array[0].length;
   
    if (format)
    {
      for (int i = 0; i < height; i++)
      {
        for (int j = 0; j < width; j++)
        {
          System.out.print(array[i][j]+" ");
        }
        System.out.println(" ");
      }
      return;
    }
    
    else
  {
    for (int i = 0; i < width; i++)
    {
      for ( int j = 0; j < height; j++)
      {
        System.out.print(array[j][i]+" ");
      }
      System.out.println(" ");
    }
  }   
  }// end of printMatrix
  
  
//   public static int addMatrix(int [][] a, boolean formata, int[][] b, boolean formatb)
//   {
//     int height = a.length;
//     int width = a[0].length;
//     if (height==width)
//     {
//       return;
//       //return null; When i take do this command, it tells me that it is not necessary. 
    
//     }//matrices cannot be added 
    
    
    
  //}//end of addMatrix
  
  

  
  

  
  
}//end of program