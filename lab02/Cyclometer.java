// CSE 2 Cyclometer.java 
// lab 02 Xaymara Rivera 2.2.18
// Purpose of Lab: To show how java can store numerical values in and perform basic computations with those variables. 
//
public class Cyclometer {
  //main method required for every Java program 
  public static void main (String[] args) {
 //out input data. Document your variables by placing your 
//comments after the "//". State the purpose of each variable. 

int secsTrip1=480; // the time (in seconds) it took for the first trip.
int secsTrip2=3220; // the time (in seconds) it took for the second trip.
int countsTrip1=1561; // the counts for the first trip. 
int countsTrip2=9037; // the counts for the second trip.
// out intermediate variables and output data. Document!
double wheelDiameter=27.0,  // States the diameter of the wheel. 
PI=3.14159, // The value of pi. 
feetPerMile=5280, // This is the unit conversion from feets to miles. 
inchesPerFoot=12, // This is the unit conversion from inches to foot. 
secondsPerMinute=60; // This is the unit conversino from seconds to minutes. 
double distanceTrip1, distanceTrip2,totalDistance; // It is saying that the values of distanceTrip1, distanceTrip2, and totalDistance are a float with extra numerical precision. 
System.out.println("Trip 1 took "+
                  (secsTrip1/secondsPerMinute)+" minutes and had "+
                  countsTrip1+" counts.");
System.out.println ("Trip 2 took " +
                   (secsTrip2/secondsPerMinute)+ " minutes and had "+
                   countsTrip2+" counts.");
//run the calculations; store the values. Document your calculations here. What are you calculating? 
// We are going to be calculating the number minutes of each trip, the number of counts on each trip, the trip of each trip in miles, and the total distance of each trips in miles. 
//
distanceTrip1=countsTrip1*wheelDiameter*PI;
// Above gives distance in inches 
//(for each count, a rotation of the wheel travels
//the diameter in inches timeS PI)
distanceTrip1/=inchesPerFoot*feetPerMile; //Gives distance in miles 
distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
totalDistance=distanceTrip1+distanceTrip2;
//Print out the output data
System.out.println("Trip 1 was "+distanceTrip1+" miles");
System.out.println("Trip 2 was "+distanceTrip2+" miles");
System.out.println("The total distance was "+totalDistance+" miles");
   
  } //end of main method 
} //end of class

