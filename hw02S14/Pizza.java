public class Pizza {
  public static void main(String[] args) {
    int sliceCountEd=3, sliceCountJan=2, sliceCountMe=2;
    int cannoliCountEd=2;
    int scoopCountJan=1, scoopCountMe=2;
    double pizzaSlice$=1.75, cannoli$=2.25, iceCreamScoop$=2.00;
      double tipPercent=0.17;
    //cost of each person's meal
    double TotalCostEd, TotalCostJan, TotalCostMe;
    TotalCostEd = (sliceCountEd * pizzaSlice$) + (cannoliCountEd * cannoli$);
    System.out.println("The total cost for Ed is " +TotalCostEd); 
    TotalCostJan = (sliceCountJan * pizzaSlice$) + (scoopCountJan * iceCreamScoop$);
    System.out.println("The total cost for Jan is "+TotalCostJan);
    TotalCostMe = (sliceCountMe * pizzaSlice$) + (scoopCountMe * iceCreamScoop$);
    System.out.println("The total cost for me is " +TotalCostMe);
    
    //total money with tip
    double TotalTip;
    double TotalMoneyTip;
    TotalTip = (TotalCostEd + TotalCostJan + TotalCostMe) * tipPercent;
    TotalMoneyTip = (TotalCostEd + TotalCostJan + TotalCostMe) + TotalTip;
    System.out.println("The total cost of everything with tip is "+TotalMoneyTip);
    
    
  }
}