//Xaymara Rivera 
//CSE 2 lab03 Check.java
//Objective: In this lab we are going to learn how to get an input from the user and use that dat to perform basic computations. 
import java.util.Scanner;
//Document your program. Place your comments here!
// In this program, we are able to use a scanner in order get the users original cost of the check, the percantage of tip they want to pay , and the number of ways that the check will be split up. 
// At the end of the program, you are able to determine the amount of money each person has to spent in order to pay the check.
public class Check {
  //main method requird for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ) ;
    System.out.print("Enter the orignal cost of the check in the form xx.xx: ") ;
    double checkCost = myScanner.nextDouble() ;
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx) : "
                    ) ;
    double tipPercent = myScanner.nextDouble() ;
    tipPercent /=100 ; //We want to convert the percentage into a decimal value.tipPercent
      System.out.print("Enter the number of people who out to dinner: ") ;
    int numPeople = myScanner.nextInt() ;
    double totalCost; 
    double costPerPerson;
    int dollars; //Whole dollar amount of cost dimes, pennies; //for storing digits
    //to the right of the decimal point
    //for the cost$
    totalCost = checkCost * (1 + tipPercent) ;
    costPerPerson = totalCost / numPeople ;
    //get the whole amount, dropping decimal fraction
    dollars = (int)costPerPerson;
    //get dimes amount, e.g.,
    //(int) (6.73 *10) % 10 -> 67 % 10 -> 7
    // where the % (mod) operator returns the remainder 
    //after the division: 583%100 -> 83, 27%5 -> 27
    int dimes = (int)(costPerPerson * 10) % 10;
    int pennies = (int)(costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    
  } //end of main method
} //end of class
