//Xaymara Rivera lab04 CSE 2
//CardGenerator
//In this program, I will be creating a random number generator. 
public class CardGenerator {
  public static void main(String[] args) {

String Suit="", CardName="";
    
    int CardNum = (int) (Math.random()*52);
    if (CardNum/13 == 0) {
      Suit = "Diamonds";
    }
    if (CardNum/13 == 1) {
      Suit = "Clubs";
    }
    if (CardNum/13 == 2) {
      Suit = "Hearts";
    }
    if (CardNum/13 == 3) {
      Suit = "Spades";
    }
    if (CardNum%13 == 1) {
      CardName = "2";
    }
    if (CardNum%13 == 2) {
      CardName = "3";
    }
    if (CardNum%13 == 0) {
      CardName = "Ace";
    }
    if (CardNum%13 == 3) {
      CardName = "4";
    }
    if (CardNum%13 == 10) {
      CardName = "Jack";
    }
    if (CardNum%13 == 11) {
      CardName = "Queen";
    }
    if (CardNum%13 == 12) {
      CardName = "King";
    }
    if (CardNum >=1 && CardNum <=9 ) {
      CardName = CardNum+"";
    }
    if (CardNum%13 == 4) {
      CardName = "5";
    }
     if (CardNum%13 == 5) {
      CardName = "6";
    }
     if (CardNum%13 == 6) {
      CardName = "7";
    }
    if (CardNum%13 == 7) {
      CardName = "8";
    }
    if (CardNum%13 == 8) {
      CardName = "9";
    }
    if (CardNum%13 == 9) {
      CardName = "10";
    }
    System.out.println("You drew the " +CardName+ " of " +Suit);
    
  }
}