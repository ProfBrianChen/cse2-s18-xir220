//CSE 2 Xaymara Rivera 
//lab02 MPG.java 
public class MPG {
  public static void main(String[] args) {
    //distance of each trip and the amount of gas for each trip
    double gasTrip1=8.2;
    double gasTrip2=13.7;
    double milesTrip1=178.0;
    double milesTrip2=342;
    System.out.println("Trip 1 covered "+milesTrip1+ " miles.");
    System.out.println("Trip 2 covered "+milesTrip2+ " miles.");
    System.out.println("Trip 1 used "+gasTrip1+ " gallons of gas.");
    System.out.println("Trip 2 used "+gasTrip2+ " gallons of gas.");
    double mpg1, mpg2, mpgTotal;
    mpg1 = milesTrip1/gasTrip1;
    mpg2 = milesTrip2/gasTrip2;
    mpgTotal = (milesTrip1+milesTrip2)/(gasTrip1+gasTrip2);
    System.out.println("The miles per gallon for trip 1 was " +mpg1);
    System.out.println("The miles per gallon for trip 2 was " +mpg2);
    System.out.println("The miles per gallon for the combined trips was "+mpgTotal);
    
  }
}
  