 import java.util.Scanner;
public class SwitchBinary {
  public static void main(String[] args) {
Scanner myScanner = new Scanner(System.in);
	int digits = 0;
    int bitValue = 0;
	System.out.print("Enter a one, two, or three bit binary number- ");
	digits = myScanner.nextInt();
	switch(digits){
	  case 111: bitValue++;
	  case 110: bitValue++;
    case 101: bitValue++;
	  case 100: bitValue++;
	  case 11: bitValue++;
	  case 10: bitValue++;
    case 1: bitValue++;
    case 0:
      break;
    default:
      System.err.println("Bad int entered");
      return;
	}
	System.out.println("The binary value is " + bitValue);
      }
  }
